package de.felixz.examples.graphql.dto

import graphql.language.StringValue
import graphql.schema.Coercing
import graphql.schema.CoercingParseLiteralException
import graphql.schema.GraphQLScalarType
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * @author felixz
 */

data class AuthData(var email: String? = null, var password: String? = null)

data class Link(val id: String?, val url: String, val description: String, val userId: String? = null)

data class SigninPayload(val token: String, val user: User)

class User(val id: String?, val name: String, val email: String, val password: String)

class Vote(val id: String?, val createdAt: ZonedDateTime, val userId: String, val linkId: String)

object Scalars {

    val dateTime = GraphQLScalarType("DateTime", "DataTime scalar", object : Coercing<ZonedDateTime, String> {

        override fun parseValue(input: Any): ZonedDateTime {
            return ZonedDateTime.parse(input.toString())
        }

        override fun parseLiteral(input: Any): ZonedDateTime {
            //parse the string values coming in
            return when (input) {
                is StringValue -> ZonedDateTime.parse(input.value)
                else -> throw CoercingParseLiteralException("literal $input was not a string")
            }
        }

        override fun serialize(dataFetcherResult: Any): String {
            return (dataFetcherResult as ZonedDateTime).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        }
    })
}

package de.felixz.examples.graphql.query

import com.fasterxml.jackson.annotation.JsonProperty
import de.felixz.examples.graphql.dto.AuthData
import de.felixz.examples.graphql.dto.Link
import de.felixz.examples.graphql.dto.SigninPayload
import de.felixz.examples.graphql.dto.User
import de.felixz.examples.graphql.dto.Vote
import de.felixz.examples.graphql.repository.LinkRepository
import de.felixz.examples.graphql.repository.VoteRepository
import de.felixz.examples.graphql.repository.UserRepository
import graphql.GraphQLException
import io.leangen.graphql.annotations.GraphQLArgument
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.annotations.GraphQLQuery
import java.time.Instant
import java.time.ZoneOffset
import javax.inject.Inject

/**
 * @author felixz
 */
class Query @Inject constructor(private val linkRepository: LinkRepository) {

    @GraphQLQuery
    fun allLinks(filter: LinkFilter?,
                 @GraphQLArgument(name = "skip", defaultValue = "0") skip: Number,
                 @GraphQLArgument(name = "first", defaultValue = "0") first: Number): List<Link> =
            linkRepository.getAllLinks(filter, skip.toInt(), first.toInt())
}

class Mutation @Inject constructor(
        private val linkRepository: LinkRepository,
        private val userRepository: UserRepository,
        private val voteRepository: VoteRepository
) {

    @GraphQLMutation
    fun createLink(@GraphQLArgument(name = "url") url: String,
                   @GraphQLArgument(name = "description") description: String): Link {
        val newLink = Link(null, url, description, null)
        linkRepository.saveLink(newLink)
        return newLink
    }

    @GraphQLMutation
    fun createUser(@GraphQLArgument(name = "url") name: String,
                   @GraphQLArgument(name = "auth") auth: AuthData): User =
            userRepository.saveUser(User(null, name, auth.email!!, auth.password!!))

    @GraphQLMutation
    @Throws(IllegalAccessException::class)
    fun signinUser(auth: AuthData): SigninPayload {
        val user = userRepository.findByEmail(auth.email!!)
        if (user!!.password == auth.password) {
            return SigninPayload(user.id!!, user)
        }
        throw GraphQLException("Invalid credentials")
    }

    @GraphQLMutation
    fun createVote(@GraphQLArgument(name = "linkId") linkId: String,
                   @GraphQLArgument(name = "userId") userId: String): Vote =
            voteRepository.saveVote(
                    Vote(null, Instant.now().atZone(ZoneOffset.UTC), userId, linkId))
}

data class LinkFilter(@get:JsonProperty("description_contains")
                      var descriptionContains: String? = null,
                      @get:JsonProperty("url_contains")
                      var urlContains: String? = null)

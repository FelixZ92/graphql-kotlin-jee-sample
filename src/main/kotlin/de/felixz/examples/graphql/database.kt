package de.felixz.examples.graphql

import com.mongodb.MongoClient
import com.mongodb.client.MongoDatabase
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.annotation.PostConstruct
import javax.ejb.Singleton
import javax.ejb.Startup
import javax.enterprise.inject.Produces
import javax.inject.Inject
import javax.inject.Qualifier
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import javax.servlet.annotation.WebListener
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.FIELD
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.annotation.AnnotationTarget.TYPE
import kotlin.annotation.AnnotationTarget.VALUE_PARAMETER

/**
 * @author felixz
 */
@Startup
@Singleton
class MongoConnection {

    private lateinit var client: MongoClient

    @get:Produces
    lateinit var database: MongoDatabase
        protected set

    @Inject
    @ConfigProperty(name = "mongo.host")
    private lateinit var mongoHost: String

    @PostConstruct
    fun init() {
        client = MongoClient(mongoHost)
        database = client.getDatabase("hackernews")
    }

    fun close() {
        client.close()
    }

    @Produces
    @LinkCollection
    fun produceLinkCollection() = database.getCollection("links")

    @Produces
    @UserCollection
    fun produceUserCollection() = database.getCollection("users")

    @Produces
    @VoteCollection
    fun produceVoteCollection() = database.getCollection("votes")
}

@Qualifier
@Retention(RUNTIME)
@Target(FIELD, TYPE, FUNCTION, VALUE_PARAMETER)
annotation class LinkCollection

@Qualifier
@Retention(RUNTIME)
@Target(FIELD, TYPE, FUNCTION, VALUE_PARAMETER)
annotation class VoteCollection

@Qualifier
@Retention(RUNTIME)
@Target(FIELD, TYPE, FUNCTION, VALUE_PARAMETER)
annotation class UserCollection

@WebListener
class MongoConnectionServlet
@Inject constructor(private var mongoConnection: MongoConnection) : ServletContextListener {

    override fun contextDestroyed(sce: ServletContextEvent?) {
        mongoConnection.close()
    }
}

package de.felixz.examples.graphql

import de.felixz.examples.graphql.query.Mutation
import de.felixz.examples.graphql.query.Query
import de.felixz.examples.graphql.resolver.LinkResolver
import graphql.ExecutionInput
import graphql.GraphQL
import io.leangen.graphql.GraphQLSchemaGenerator
import java.io.StringReader
import javax.annotation.PostConstruct
import javax.ejb.Singleton
import javax.ejb.Startup
import javax.enterprise.inject.Produces
import javax.inject.Inject
import javax.json.Json
import javax.ws.rs.ApplicationPath
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Application
import javax.ws.rs.core.MediaType

/**
 * @author felixz
 */
@Startup
@Singleton
class GraphQLSchemaProvider {

    @Inject
    private lateinit var query: Query

    @Inject
    private lateinit var linkResolver: LinkResolver

    @Inject
    private lateinit var mutation: Mutation

    @get:Produces
    lateinit var graphQL: GraphQL
        protected set

    @PostConstruct
    fun init() {
        val schema = GraphQLSchemaGenerator()
                .withOperationsFromSingletons(query, linkResolver, mutation)
                .generate()

        graphQL = GraphQL.newGraphQL(schema).build()
    }
}

@Path("/")
class GraphQLResource @Inject constructor(private val graphQL: GraphQL) {

    @GET
    @javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
    fun executeGetQuery(@QueryParam("query") query: String): Map<String, Any> {
        val input = ExecutionInput.newExecutionInput().query(query).build()
        val result = graphQL.execute(input)
        return result.toSpecification()
    }

    @POST
    @javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun executePostQuery(requestBody: String): Map<String, Any> {
        val jsonObject = Json.createReader(StringReader(requestBody)).readObject()

        val input = ExecutionInput.newExecutionInput()
                .query(jsonObject.getString("query"))
                .build()
        val result = graphQL.execute(input)
        return result.toSpecification()
    }
}

@ApplicationPath("/graphql")
class JaxRsConfig : Application()

package de.felixz.examples.graphql.repository

import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import de.felixz.examples.graphql.LinkCollection
import de.felixz.examples.graphql.UserCollection
import de.felixz.examples.graphql.VoteCollection
import de.felixz.examples.graphql.dto.Link
import de.felixz.examples.graphql.dto.Scalars
import de.felixz.examples.graphql.dto.User
import de.felixz.examples.graphql.dto.Vote
import de.felixz.examples.graphql.query.LinkFilter
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.types.ObjectId
import java.time.ZonedDateTime
import java.util.ArrayList
import java.util.Optional
import javax.inject.Inject

/**
 * @author felixz
 */

class LinkRepository @Inject constructor(@LinkCollection private val links: MongoCollection<Document>) {

    fun findById(id: String): Link {
        return link(links.find(Filters.eq<ObjectId>("_id", ObjectId(id))).first())
    }

    fun getAllLinks(filter: LinkFilter?, skip: Int, first: Int): List<Link> {
        val mongoFilter = Optional.ofNullable(filter).map(this::buildFilter)

        val allLinks = ArrayList<Link>()
        val documents = mongoFilter.map(links::find).orElseGet(links::find)
        documents.skip(skip).limit(first).toList()
        for (doc in documents.skip(skip).limit(first)) {
            allLinks.add(link(doc))
        }
        return allLinks
    }

    fun saveLink(link: Link) {
        links.insertOne(
                Document().apply {
                    append("url", link.url)
                    append("description", link.description)
                    append("postedBy", link.userId)
                }
        )
    }

    private fun buildFilter(filter: LinkFilter): Bson? {
        val descriptionPattern = filter.descriptionContains
        val urlPattern = filter.urlContains
        val descriptionCondition = if (descriptionPattern.isNullOrEmpty()) {
            Filters.regex("description", ".*$descriptionPattern.*", "i")
        } else null
        val urlCondition = if (urlPattern.isNullOrEmpty()) {
            Filters.regex("url", ".*$urlPattern.*", "i")
        } else null

        return if (descriptionCondition != null && urlCondition != null) {
            Filters.and(descriptionCondition, urlCondition)
        } else descriptionCondition ?: urlCondition
    }

    private fun link(doc: Document): Link {
        return Link(
                doc["_id"].toString(),
                doc.getString("url"),
                doc.getString("description"),
                doc.getString("postedBy"))
    }
}

class UserRepository @Inject constructor(@UserCollection private val users: MongoCollection<Document>) {

    fun findByEmail(email: String): User? = user(users.find(Filters.eq<String>("email", email)).first())

    fun findById(id: String): User? = user(users.find(Filters.eq<ObjectId>("_id", ObjectId(id))).first())

    fun saveUser(user: User): User {
        val doc = Document().apply {
            append("name", user.name)
            append("email", user.email)
            append("password", user.password)
        }
        users.insertOne(doc)
        return User(
                doc["_id"].toString(),
                user.name,
                user.email,
                user.password)
    }

    private fun user(doc: Document?): User? {
        return if (doc != null) User(doc["_id"].toString(),
                doc.getString("name"),
                doc.getString("email"),
                doc.getString("password"))
        else null
    }
}

class VoteRepository @Inject constructor(@VoteCollection private var votes: MongoCollection<Document>) {

    fun findByUserId(userId: String): List<Vote> =
            votes.find(Filters.eq<String>("userId", userId)).map { vote(it) }.toList()

    fun findByLinkId(linkId: String): List<Vote> =
            votes.find(Filters.eq<String>("linkId", linkId)).map { vote(it) }.toList()

    fun saveVote(vote: Vote): Vote {
        val doc = Document().apply {
            append("userId", vote.userId)
            append("linkId", vote.linkId)
            append("createdAt", Scalars.dateTime.coercing.serialize(vote.createdAt))
        }
        votes.insertOne(doc)
        return Vote(
                doc["_id"].toString(),
                vote.createdAt,
                vote.userId,
                vote.linkId)
    }

    private fun vote(doc: Document): Vote =
            Vote(doc["_id"].toString(),
                    ZonedDateTime.parse(doc.getString("createdAt")),
                    doc.getString("userId"),
                    doc.getString("linkId"))
}

fun main(args: Array<String>) {
    val s : String? = null
    println("s.isNullOrBlank() = ${s!!.isBlank()}")
}


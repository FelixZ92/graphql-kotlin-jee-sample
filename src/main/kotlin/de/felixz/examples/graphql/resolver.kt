package de.felixz.examples.graphql.resolver

import com.coxautodev.graphql.tools.GraphQLResolver
import de.felixz.examples.graphql.dto.Link
import de.felixz.examples.graphql.dto.SigninPayload
import de.felixz.examples.graphql.dto.User
import de.felixz.examples.graphql.dto.Vote
import de.felixz.examples.graphql.repository.LinkRepository
import de.felixz.examples.graphql.repository.UserRepository
import io.leangen.graphql.annotations.GraphQLContext
import io.leangen.graphql.annotations.GraphQLQuery
import javax.inject.Inject

/**
 * @author felixz
 */
class LinkResolver @Inject constructor(private val userRepository: UserRepository) {

    @GraphQLQuery
    fun postedBy(@GraphQLContext link: Link): User? =
            when {
                link.userId == null -> null
                else -> userRepository.findById(link.userId)
            }
}

class SigninResolver : GraphQLResolver<SigninPayload> {

    fun user(payload: SigninPayload): User = payload.user
}

class VoteResolver @Inject constructor(
        private val linkRepository: LinkRepository,
        private val userRepository: UserRepository) : GraphQLResolver<Vote> {

    fun user(vote: Vote): User? = userRepository.findById(vote.userId)

    fun link(vote: Vote): Link = linkRepository.findById(vote.linkId)
}

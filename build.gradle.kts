plugins {
    id("org.jetbrains.kotlin.jvm").version("1.2.71")
    id("org.jetbrains.kotlin.plugin.allopen").version("1.2.71")
    id("org.jetbrains.kotlin.plugin.noarg").version("1.2.71")
    id("war")
}

group = "de.felixz.examples.graphql"
version = "1.0.0"

repositories {
    jcenter()
}

val payaraJar by configurations.creating

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.2.71")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.2.71")
    implementation("com.graphql-java:graphql-java:11.0")
    implementation("com.graphql-java-kickstart:graphql-java-tools:5.3.3")
    implementation("org.mongodb:mongodb-driver:3.4.3")
    implementation("io.leangen.graphql:spqr:0.9.7")

    compileOnly(group = "javax", name = "javaee-api", version = "8.0")
    compileOnly(group = "org.eclipse.microprofile.config", name = "microprofile-config-api", version = "1.3")

    testImplementation(group = "junit", name = "junit", version = "4.12")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.2.71")
    testImplementation("org.jetbrains.kotlin:kotlin-test:1.2.71")

    payaraJar("fish.payara.extras:payara-micro:5.183")
}

allOpen {
    annotation("javax.servlet.annotation.WebListener")
    annotation("javax.ejb.Singleton")
    annotation("javax.annotation.PostConstruct")
    annotation("javax.ejb.Stateless")
    annotation("javax.persistence.Entity")
}

val war by project.tasks.getting(War::class) {
    version = "${project.version}"
    archiveName = "hackernews-graphql-java.war"
}

val runMicro by tasks.creating(Exec::class) {
    dependsOn(war)
    val payaraJarPath = configurations["payaraJar"]
            .first { it -> it.name.contains("payara-micro") }
            .absolutePath
    val warFile = war.archivePath
    commandLine("java", "-jar", payaraJarPath, "--deploy", warFile)
}

val stage by tasks.creating(Task::class) {
    dependsOn(war)
}

# graphql-kotlin-jee-sample

This project implements a JAX-RS resource exposing a GraphQL endpoint.
It is based on the [graphql-java-tutorial](https://github.com/howtographql/graphql-java).

It uses MongoDB as a datastore, so make sure you have it running on `localhost:27017`, e.g. via docker:

`docker run --name graphql-kotlin-jee-sample-mongo -p 27017:27017 -d mongo:3.2-jessie`

To build and deploy on payara-micro run:

`./gradlew runMicro`
